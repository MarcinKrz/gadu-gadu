#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/ipc.h>
#include <sys/sem.h>


#define QUEUE_SIZE 100
#define clientsNumber 100
#define MAXSIZE 30
#define Start_Message 2
#define End_Message 25
#define connect_message_size 138
#define send_message_size 2015
#define contact_size 73


struct message_structure  //do przechowywania wiadomosci
{
    int num_cust_from;
    int num_cust_to;
    char message[2000];
};

struct contact // dane o kliencie
{
    int number;
    char name[64];
    bool active; //czy aktywny w tym momencie czy nie
};

struct contact_with_password
{
    struct contact contact;
    char password[64];
};

struct contact_with_desc
{
    struct contact_with_password con_pass;
    int desc;
    pthread_mutex_t mutex ;
};


int find_client(struct contact_with_desc * contact, int size, int client_number);
int isempty(int size[],int elem);
int isfull(int size[], int elem, int maxsize);
void enqueue(struct message_structure value,int elem, int size[], int maxsize, struct message_structure queue[][maxsize], int front[], int rear[]);
struct message_structure dequeue(int size[], int elem,int maxsize, int front[],struct message_structure queue[][maxsize]);
char * extract_text(char * text,int from, int how_much);
void fill_message_buf(char *buf,char start,int cust_from, int cust_to, char* message, char stop,int size);


struct contact_with_desc client_contact_list[clientsNumber];
struct message_structure queue [clientsNumber][MAXSIZE];
int size[clientsNumber];
int front[clientsNumber];
int rear[clientsNumber];
pthread_mutex_t mutex[3]; //0 -mutex na tablice kontaktow; 1 - mutex na kolejke (z wiadomosciami), 2 mutex na numberOfClients i lastClientNumber
int numberOfClients;
int lastClientNumber;

//funkcja obsługująca połączenie z nowym klientem
int clientConnection(int connection_socket_descriptor)
{
    char z=0,connectError='0',marker=0;
    int condition = 1,condition1=1;
    int counter;
    int read_return;
    int clientNumber;
    bool thread_cr = true;
    bool send_message = false;
    bool which_return = false;
    int act_num_cli;
    int error;
    struct message_structure ms;
    FILE *fptr;
    char *pom = (char *)malloc (connect_message_size);
    char *send_pom = (char *)malloc(send_message_size);

    memset(pom,0,connect_message_size);

    //dopóki klient nie zaloguje/rejestruje się poprawnie lub nie wyjdzie
    while(condition1)
    {
        condition=1;
        memset(pom,0,connect_message_size);
        counter = 0;

        //czytanie wiadomosci początkowej
        while(condition)
        {
            read_return = read(connection_socket_descriptor,&z,sizeof(z));
            if(z==Start_Message)
            {
                condition=2;
            }
            if(condition == 2)
            {
                if(counter<connect_message_size)
                {
                    pom[counter]=z;
                    counter++;
                }

                if(z==End_Message)
                {
                    condition = 0;
                }

            }

            if(read_return <= 0)
                condition=0;
        }

        if(read_return>0 && counter == connect_message_size)
        {
            if(pom[1] == 'r')
            {//rejestracja gdy przyszla poprawna wiadomosc 
                pthread_mutex_lock(&mutex[2]);
                //sprawdzanie czy mozna jeszcze kogos zarejestrowac (czy liczba klientow nie jest juz maksymalna)
                if(numberOfClients < clientsNumber)
                {

                    act_num_cli = numberOfClients;
                    pthread_mutex_init(&client_contact_list[act_num_cli].mutex,NULL);
                    pthread_mutex_lock(&mutex[0]);
                    lastClientNumber +=1;

                    //przepis danych klienta do client_contact_list (tablicy do przechowywania danych o klientach)
                    strcpy(client_contact_list[act_num_cli].con_pass.contact.name, extract_text(pom,9,64));
                    client_contact_list[act_num_cli].con_pass.contact.active = true;
                    client_contact_list[act_num_cli].con_pass.contact.number = lastClientNumber;
                    strcpy(client_contact_list[act_num_cli].con_pass.password, extract_text(pom,73,136));
                    client_contact_list[act_num_cli].desc = connection_socket_descriptor;

                    connectError = '0';
                    clientNumber = lastClientNumber;
                    thread_cr=true;
                    which_return=true;

                    //zapis danych nowego klienta do pliku
                    if ((fptr = fopen("client_data.cldata","ab+")) == NULL)
                    {
                        printf("Error! opening file");

                        exit(1);
                    }
                    fseek(fptr,0,SEEK_END);
                    client_contact_list[act_num_cli].con_pass.contact.active = false;
                    fwrite(&client_contact_list[act_num_cli].con_pass, sizeof(struct contact_with_password), 1, fptr);
                    client_contact_list[act_num_cli].con_pass.contact.active = true;
                    fclose(fptr);
                    pthread_mutex_unlock(&mutex[0]);
                    numberOfClients+=1;
                }
                else
                {
                    //uzupelnienie danych do odeslania z brakiem mozliwosci rejestracji
                    connectError = '1';
                    which_return=false;
                    thread_cr=false;
                }

                pthread_mutex_unlock(&mutex[2]);

            }
            else if(pom[1]== 'l')
            {//logowanie gdy przyszla poprawna wiadomosc 
                pthread_mutex_lock(&mutex[0]);
                clientNumber = atoi(extract_text(pom,3,6));
                act_num_cli = find_client(client_contact_list,clientsNumber,clientNumber);

                //sprawdzanie czy numer gg jest poprawny
                if(act_num_cli>=0)
                {
                	//jesli numer gg jest poprawny, sprawdzanie czy ten uzytkownik nie jest juz w systemie zalogowany
                    if(client_contact_list[act_num_cli].con_pass.contact.active == false)
                    {
                    	//jesli numer gg jest poprawny i uzytkownik nie jest jeszcze zalogowany, sprawdzanie czy haslo jest poprawne
                        if(strcmp(extract_text(pom,73,64),client_contact_list[act_num_cli].con_pass.password)==0)
                        {
                            connectError = '0';
                            pthread_mutex_init(&client_contact_list[act_num_cli].mutex,NULL);
                            client_contact_list[act_num_cli].con_pass.contact.active = true;
                            client_contact_list[act_num_cli].desc = connection_socket_descriptor;
                            send_message=true;
                            which_return=true;
                            thread_cr=true;
                        }
                        else
                        {
                            //niepoprawne haslo
                            connectError = '2';
                            which_return=false;
                            thread_cr = false;
                        }
                    }
                    else
                    {
                        //juz ten klient jest zalogowany
                        connectError = '4';
                        which_return=false;
                        thread_cr = false;
                    }
                }
                else
                {
                    //niepoprawne id
                    connectError = '3';
                    which_return=false;
                    thread_cr = false;
                }
                pthread_mutex_unlock(&mutex[0]);
            }
            marker = pom[1];

            memset(pom,0,connect_message_size);
            //uzupelnienie wiadomosci do odeslania
            pom[0]=Start_Message;
            pom[1]= marker;
            pom[2]=connectError;
            sprintf(&pom[3],"%d",clientNumber);
            pom[connect_message_size-1]=End_Message;
            //odsylanie wstepnej wiadomosci do uzytkownika
            while((error=write(connection_socket_descriptor,pom,connect_message_size))!=connect_message_size)
            {
                if(error<0)
                {
                    printf("Error");
                    thread_cr = false;
                    which_return=false;
                    condition1=0;
                    break;
                }
                z=End_Message;
                write(connection_socket_descriptor,&z,sizeof(z));
                z=0;
            }
            
            if(thread_cr)
            {
                memset(&ms,0,sizeof(ms));
                //sprawdzanie czy moga byc jakies wiadomosci (moga gdy klient sie loguje poprawnie)
                if(send_message)
                {
                    pthread_mutex_lock(&client_contact_list[act_num_cli].mutex);
                    pthread_mutex_lock(&mutex[1]);
                    //sprawdzanie czy sa jakies wiadomosci do tego klienta (czy lista nie jest pusta)
                    if(!isempty(size,act_num_cli))
                    {
                    	//wysylanie wiadomosci do klienta dopóki lista wiadomosci (ktore otrzymał gdy go nie było), nie jest pusta
                        while(!isempty(size,act_num_cli))
                        {
                            ms = dequeue(size,act_num_cli,MAXSIZE,front,queue);
                            memset(send_pom,0,send_message_size);
                            fill_message_buf(send_pom,Start_Message,ms.num_cust_from,ms.num_cust_to,ms.message,End_Message,send_message_size);
                            while((error=write(connection_socket_descriptor,send_pom,send_message_size))!=send_message_size)
                            {
                                if(error<0)
                                {
                                    printf("Error");
                                    which_return=false;
                                    break;
                                }
                                z=End_Message;
                                write(connection_socket_descriptor,&z,sizeof(z));
                                z=0;

                            }
                        }
                    }
                    pthread_mutex_unlock(&mutex[1]);
                    pthread_mutex_unlock(&client_contact_list[act_num_cli].mutex);
                }
                condition1=0;

            }

        }
        else
        {
        	//jezeli byl blad, lub gniazdo zamkniete
            if(read_return<=0)
            {
                close(connection_socket_descriptor);
                printf("Połączenie z klientem zostało przerwane \n");
                which_return=false;
                condition1=0;
            }
        }
    }
    free(send_pom);
    free(pom);
    //jezeli uzytkownik zalogowal/zarejestrowal sie pomyslnie, zwracany jest jego identyfikator w tablicy z klientami
    if(which_return)
    {
        return act_num_cli;
    }
    else
    {
        return -1;
    }
}

//watek odpowiedzialny za obsluge pojedynczego uzytkownika
void *Communication(void *data)
{
    pthread_detach(pthread_self());

    int act_num_cli;
    int desc = *(int *) data;
    int read_return=1;
    int error;
    struct message_structure ms;
    char z = 0;
    char *pom = (char *)malloc (send_message_size);
    char *contact_buf = (char *)malloc (contact_size);
    int num_second_client;
    int counter,condition=1;

    //wywolywanie funkcji odpowiadajacej za logowanie/rejestracje, jezeli zwraca numer uzytkownika w tablicy, wykonywane sa dalsze operacje, jesli nie watek klienta jest zamykany
    if((act_num_cli = clientConnection(desc))>=0)
    {


        while(read_return > 0)
        {
            counter = 0;
            memset(pom,0,send_message_size);
            condition=1;

            // odbieranie wiadomosci od klienta
            while(condition > 0)
            {

                read_return = read(desc,&z,sizeof(z));

                if(z==Start_Message)
                {
                    condition=2;
                }
                if(condition == 2)
                {
                    if(counter<send_message_size)
                    {
                        pom[counter]=z;
                        counter++;
                    }

                    if(z==End_Message)
                    {
                        condition = 0;
                    }

                }

                if(read_return <= 0)
                    condition=0;
            }

            //przetwarzanie wiadomosci klienta
            if(read_return>0 && counter == send_message_size)
            {
                if(pom[1] == '1' )
                {
                    //1) wysylanie komus wiadomosci, jak go nie ma wrzucanie do kolejki
                    pthread_mutex_lock(&mutex[0]);
                    num_second_client = find_client(client_contact_list,clientsNumber,atoi(extract_text(pom,8,6)) );
                    if(num_second_client >= 0)
                    {
                    	//sprwadzanie czy aktywwny
                        if(client_contact_list[num_second_client].con_pass.contact.active == true)
                        {

                            pthread_mutex_lock(&client_contact_list[num_second_client].mutex);
                            //wysylanie wiadomosci
                            while((error=write(client_contact_list[num_second_client].desc,pom,send_message_size))!=send_message_size)
                            {
                                if(error<0)
                                {
                                    printf("Error");
                                    break;
                                }
                                z=End_Message;
                                write(client_contact_list[num_second_client].desc,&z,sizeof(z));
                                z=0;

                            }

                            pthread_mutex_unlock(&client_contact_list[num_second_client].mutex);
                        }
                        else
                        {
                            pthread_mutex_lock(&mutex[1]);
                            //jesli nie pełna kolejka, dokladanie nowej wiadomosci
                            if(!isfull(size,num_second_client,MAXSIZE))
                            {
                                ms.num_cust_from = atoi(extract_text(pom,2,6));
                                ms.num_cust_to = atoi(extract_text(pom,8,6));
                                strcpy(ms.message,extract_text(pom,14,2000));
                                enqueue(ms,num_second_client,size,MAXSIZE,queue,front,rear);
                            }
                            pthread_mutex_unlock(&mutex[1]);
                        }
                    }
                    pthread_mutex_unlock(&mutex[0]);

                }
                else if(pom[1] == '2')
                {
                    //2)wysylanie temu samemu listy kontatkow
                    pthread_mutex_lock(&mutex[0]);
                    int i;
                    pthread_mutex_lock(&client_contact_list[act_num_cli].mutex);
                    for(i=0; i<clientsNumber; i++)
                    {
                        if(client_contact_list[i].con_pass.contact.number!=0)
                        {
                            memset(contact_buf,0,contact_size);
                            contact_buf[0]=Start_Message;
                            sprintf(&contact_buf[1],"%d",client_contact_list[i].con_pass.contact.number);
                            strcpy(&contact_buf[7],client_contact_list[i].con_pass.contact.name);
                            if(client_contact_list[i].con_pass.contact.active ==true)
                                contact_buf[71]= 'a';
                            else
                                contact_buf[71]='n';
                            contact_buf[contact_size-1]=End_Message;

                            while((error=write(desc,contact_buf,contact_size))!=contact_size)
                            {
                                if(error<0)
                                {
                                    printf("Error");
                                    break;
                                }
                                z=End_Message;
                                write(client_contact_list[num_second_client].desc,&z,sizeof(z));
                                z=0;

                            }
                        }
                    }
                    pthread_mutex_unlock(&client_contact_list[act_num_cli].mutex);
                    pthread_mutex_unlock(&mutex[0]);
                }

            }
            else
            {
                pthread_mutex_lock(&mutex[0]);
                client_contact_list[act_num_cli].con_pass.contact.active = false;
                pthread_mutex_unlock(&mutex[0]);
            }

        }
    }

    close(desc);
    free(pom);
    free(contact_buf);
    pthread_mutex_destroy(&client_contact_list[act_num_cli].mutex);
    pthread_exit(NULL);

}

int main(int argc, char* argv[])
{
    int server_socket_descriptor;
    int connection_socket_descriptor;
    int bind_result;
    int listen_result;
    char reuse_addr_val = 1;
    struct sockaddr_in server_address;
    FILE *fptr;
    int i;
    int create_result = 0;
    pthread_t thread1;
    numberOfClients = 0;
    lastClientNumber = 132355;

    if (argc != 3)
    {
        fprintf(stderr, "Sposób użycia: %s server_name port_number\n", argv[0]);
        exit(1);
    }
    //czyszczenie tablic i inicjalizacja mutexow
    memset(&size,-1,(sizeof(size)));
    memset(&front,-1,(sizeof(front)));
    memset(&rear,-1,(sizeof(rear)));
    for (i=0; i < clientsNumber; i++)
        memset(&queue[i],0,(sizeof(queue[i])));


    for(i=0; i<3; i++)
    {
        pthread_mutex_init(&mutex[i],NULL);
    }


    //inicjalizacja gniazda serwera
    memset(&server_address, 0, sizeof(struct sockaddr));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = inet_addr(argv[1]);
    if(server_address.sin_addr.s_addr==-1)
    {
        fprintf(stderr, "%s: Błąd przy próbie nadania adresu\n", argv[1]);
        exit(1);

    }
    server_address.sin_port = htons(atoi(argv[2]));

    server_socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_descriptor < 0)
    {
        fprintf(stderr, "%s: Błąd przy próbie utworzenia gniazda..\n", argv[0]);
        exit(1);
    }
    setsockopt(server_socket_descriptor, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse_addr_val, sizeof(reuse_addr_val));

    bind_result = bind(server_socket_descriptor, (struct sockaddr*)&server_address, sizeof(struct sockaddr));
    if (bind_result < 0)
    {
        fprintf(stderr, "%s: Błąd przy próbie dowiązania adresu IP i numeru portu do gniazda.\n", argv[0]);
        exit(1);
    }

    listen_result = listen(server_socket_descriptor, QUEUE_SIZE);
    if (listen_result < 0)
    {
        fprintf(stderr, "%s: Błąd przy próbie ustawienia wielkości kolejki.\n", argv[0]);
        exit(1);
    }



    memset(&client_contact_list, 0, sizeof(struct contact_with_desc)*100);
    //czytanie kont klientow z pliku
    if ((fptr = fopen("client_data.cldata","r")))
    {


        while(fread(&client_contact_list[numberOfClients].con_pass, sizeof(struct contact_with_password), 1, fptr)>0)
        {
            lastClientNumber=client_contact_list[numberOfClients].con_pass.contact.number;
            numberOfClients+=1;
        }

        fclose(fptr);
    }
    else
    {
        printf("File doesn't exists, so it wasn't any clients yet. \n");
    }



    while(1)
    {
        connection_socket_descriptor = accept(server_socket_descriptor, NULL, NULL);
        if (connection_socket_descriptor < 0)
        {
            fprintf(stderr, "%s: Błąd przy próbie utworzenia gniazda dla połączenia.\n", argv[0]);
            exit(1);
        }

        //wywoływanie wątku dla zaakceptowanego połączenia
        create_result = pthread_create(&thread1, NULL, Communication, (void *) &connection_socket_descriptor);
        if (create_result)
        {
            printf("Błąd przy próbie utworzenia wątku, kod błędu: %d\n", create_result);
            exit(-1);
        }

    }

    for(i=0; i<3; i++)
    {
        pthread_mutex_destroy(&mutex[i]);
    }
    close(server_socket_descriptor);
    return(0);
}


int find_client(struct contact_with_desc * contact, int size, int client_number)
{
    //function return client number in array, if not client on array, function return -1
    int i;
    int client_num_in_arr = -1;
    for(i=0; i<size; i++)
    {
        if(contact[i].con_pass.contact.number == client_number)
            client_num_in_arr = i;
    }
    return client_num_in_arr;
}

int isempty(int size[],int elem)
{
    return size[elem] <= 0;  //0 jak niepusta
}

int isfull(int size[], int elem, int maxsize)
{
    return size[elem] == maxsize; //0 jak niepełna
}

void enqueue(struct message_structure value,int elem, int size[], int maxsize, struct message_structure queue[][maxsize], int front[], int rear[])
{
    if(size[elem] < maxsize)
    {
        if(size[elem] < 0)
        {
            queue[elem][0] = value;
            front[elem] = rear[elem] = 0;
            size[elem] = 1;
        }
        else if(rear[elem] == maxsize-1)
        {
            queue[elem][0] = value;
            rear[elem] = 0;
            size[elem]++;
        }
        else
        {
            queue[elem][rear[elem]+1] = value;
            rear[elem]++;
            size[elem]++;
        }
    }
    else
    {
        printf("Queue is full\n");
    }
}

struct message_structure dequeue(int size[], int elem,int maxsize, int front[],struct message_structure queue[][maxsize])
{
    struct message_structure ret_elem;
    if(size[elem] < 0)
    {
        printf("Queue is empty\n");
    }
    else
    {

        ret_elem = queue[elem][front[elem]];
        memset(&queue[elem][front[elem]],0,sizeof(queue[elem][front[elem]]));
        size[elem]--;
        front[elem]++;
    }
    return ret_elem;
}

char * extract_text(char * text,int from, int how_much)
{
    char * result;
    result = malloc(how_much);
    for(int i=0; i<how_much; i++)
        result[i]= text[from+i];
    return result;
}

void fill_message_buf(char *buf,char start,int cust_from, int cust_to, char* message, char stop,int size)
{
    buf[0]=start;
    sprintf(&buf[2],"%d",cust_from);
    sprintf(&buf[8],"%d",cust_to);
    strcpy(&buf[14],message);
    buf[size-1]=stop;
}
