package gadugadu;

import javafx.application.Platform;
import gadugadu.controllers.Contact;
import gadugadu.controllers.ConversationController;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * klasa  uruchamiana jako wątek w celu czytania danych przysyłanych przez serwer
 */
public class ClientRead extends Thread implements Serializable {

    Socket clientSocket;
    InputStream is = null;

    public ClientRead(Socket clientSocket) {
        this.clientSocket = clientSocket;

        try {
            is = clientSocket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    int read_count;
    int x;
    int counter = 0;
    char[] buffer = new char[2015];
    byte[] read_char = new byte[1];
    int value_response;
    int ERR_INDEX = 2;
    int MARKER_INDEX = 1;
    int NUMBER_START = 3;
    int NUMBER_END = 8;
    int NAME_START = 9;
    int NAME_END = 72;


    private volatile AtomicBoolean threadWork = new AtomicBoolean(true);

    public void endConnection(){
        this.threadWork.set(false);
        Main.client.getClientRead().stop();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Main.client.closeConnection();

    }

    /**
     * metoda wykonywana przez wątek służąca do czytania wiadomości przysyłanych z serwera
     */
    @Override
    public void run() {
        while (threadWork.get()) {
            try {
                x = is.read(read_char);
            } catch (IOException e) {
            }
            if (x == -1) {
                System.out.println("Serwer niespodziewanie zakończył połączenie, uruchom aplikację ponownie, aby spróbować nawiązać nowe połączenie");
                if(Main.client.contactsController != null){
                    Main.client.contactsController.stopTask();
                }
                endConnection();
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                }
            }

            String response = new String(read_char);
            value_response = (int) response.charAt(0);

            if (value_response == 2) { //poczatek komunikatu
                counter = 0;
                buffer = new char[2015];
                buffer[counter] = response.charAt(0);
            } else {
                counter++;
                if (counter >= 2015) {
                    counter = 0;
                    buffer = new char[2015];
                } else {
                    buffer[counter] = response.charAt(0);
                }
            }


            if ((int) buffer[0] == 2 && (int) buffer[counter] == 25) {
                if (counter == 137) {
                    if (buffer[MARKER_INDEX] == 'r' && buffer[ERR_INDEX] == '1') {
                        Platform.runLater(() -> Main.client.registrationController.setWarning("Za dużo klientów"));
                    } else if (buffer[MARKER_INDEX] == 'r' && buffer[ERR_INDEX] == '0') {
                        String numb = new String(buffer).substring(3, 9);
                        Platform.runLater(() -> Main.client.registrationController.createContactWindow(
                                numb));
                    } else if (buffer[MARKER_INDEX] == 'l' && buffer[ERR_INDEX] == '0') {
                        String numb = new String(buffer).substring(3, 9);
                        Platform.runLater(() -> Main.client.logInController.createContactWindow(
                                numb));
                    } else if (buffer[MARKER_INDEX] == 'l' && buffer[ERR_INDEX] == '2') {
                        Platform.runLater(() -> Main.client.logInController.setWarning("Podane hasło jest nieprawidłowe"));
                    } else if (buffer[MARKER_INDEX] == 'l' && buffer[ERR_INDEX] == '3') {
                        Platform.runLater(() -> Main.client.logInController.setWarning("Nie istnieje użytkownik o podanym numerze"));
                    } else if (buffer[MARKER_INDEX] == 'l' && buffer[ERR_INDEX] == '4') {
                        Platform.runLater(() -> Main.client.logInController.setWarning("Użytkownik o podanym numerze jest już zalogowany w serwisie!"));
                    } else
                        System.out.println("komunikat zajmujacy 137 bajtow jest niepoprawny");

                } else if (counter == 2014) {
                    int numberFrom = Integer.parseInt(new String(buffer).substring(2, 8));
                    String message = new String(buffer).substring(14, 2014);
                    Contact data_from = Main.client.getContacts().stream()
                            .filter(person -> person.getNumber() == numberFrom )
                            .findAny()
                            .orElse(new Contact(numberFrom,"Other person: ",true));


                    if (Main.client.controllerMap.containsKey(data_from)) {
                        ConversationController conversationController = Main.client.controllerMap.get(data_from);
                        Platform.runLater(() -> conversationController.showNewMessage(message));
                    } else {
                        Platform.runLater(() -> Main.client.contactsController.newConversationWindow(data_from));
                        while (!Main.client.controllerMap.containsKey(data_from)) {
                            try {
                                Thread.sleep(5);
                            } catch (InterruptedException e) {
                            }
                        }
                        Main.client.controllerMap.get(data_from).showNewMessage(message);
                    }
                } else if (counter == 72) {
                    int number = Integer.parseInt(new String(buffer).substring(1, 7));
                    String name =new String(buffer).substring(7, 71).replace("\0", "");
                    boolean status = buffer[71] == 'a' ? true : false;
                    Contact newContact = new Contact(number,name,status);
                    Main.client.contactsController.addNewContact(newContact);
                }
                buffer = new char[2015];
                counter = 0;

            }

        }
    }

}

