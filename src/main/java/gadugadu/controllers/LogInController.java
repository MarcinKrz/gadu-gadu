package gadugadu.controllers;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import gadugadu.Main;

import java.io.IOException;

public class LogInController {

    @FXML
    private  TextField numberTextField;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private Label warningLabel;

    public void setWarning(String warning) {
        warningLabel.setVisible(true);
        warningLabel.setWrapText(true);
        warningLabel.setText("Nieudana próba logowania!\n" + warning);
    }


    public void createContactWindow(String number) {
        Main.client.setMe(new Contact(Integer.parseInt(number),new String("me"),true));
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("contactstList.fxml"));

        try {
            fxmlLoader.load();
        } catch (IOException e) {
            System.out.println("asdas bład");
        }


        Parent parent = fxmlLoader.getRoot();

        Scene scene = new Scene(parent, 403, 400);
        Stage stage = new Stage();
        stage.setOnCloseRequest(e -> {
            Main.client.contactsController.stopTask();
            Main.client.getClientRead().endConnection();
            Platform.exit();
        });
        stage.setTitle(number);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();

        Stage stageLogIn = (Stage) numberTextField.getScene().getWindow();
        stageLogIn.close();
        Main.client.logInController=null;
    }

    public void handleLogIn(ActionEvent event) throws IOException {

        char[] number = numberTextField.getText().toCharArray();
        char[] password = passwordTextField.getText().toCharArray();
        boolean data_good = false;

        warningLabel.setText("");
        warningLabel.setVisible(false);

        if (number.length != 6) {
            System.out.println(number.length);
            warningLabel.setVisible(true);
            warningLabel.setWrapText(true);
            warningLabel.setText("Nieudane logowanie!\nNumer identyfikacyjny musi mieć długość 6 znaków");
        } else if (password.length > 64) {
            warningLabel.setVisible(true);
            warningLabel.setWrapText(true);
            warningLabel.setText("Nieudane logowanie!\nMaksymalna długość hasła powinna wynosić 64 znaki");
        } else
            data_good = true;


        if (data_good) {
            Main.client.logIn(number, password);
        }

    }


    @FXML
    public void initialize() {
        Main.client.logInController = this;
        numberTextField.textProperty().addListener(new ChangeListener<String>() {

            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    numberTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }


    public void createNewAccount(ActionEvent event) throws IOException {

        Parent parent = FXMLLoader.load(getClass().getClassLoader().getResource("registration.fxml"));

        Scene scene = new Scene(parent, 400, 400);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setOnCloseRequest(e -> {
            Main.client.getClientRead().endConnection();
            Platform.exit();
        });
        stage.show();
        Main.client.logInController=null;

    }

    public void end(){
        Platform.exit();
    }


}


