package gadugadu.controllers;


import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * klasa odzwierciedlająca dane użytkownika gadu-gadu
 */
@Getter
public class Contact {
    @Setter
    int number;
    @Setter
    String name;

    boolean active;
    String status;

    public void setActive(boolean active) {
        this.active = active;
        status = "Niedostępny";
    }

    public void setStatus(String status) {
        this.status = status;
        if (status.equals("Niedostępny")) {
            active = false;
        } else if (status.equals("Dostępny")) {
            active = true;
        }
    }

    public Contact(int number, String name, boolean active) {
        this.number = number;
        this.name = name;
        this.active = active;
        status = "Niedostępny";
        if (active) {
            status = "Dostępny";
        }
    }

    @Override
    public String toString() {
        return name + " " + status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return number == contact.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
