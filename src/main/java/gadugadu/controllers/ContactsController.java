package gadugadu.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import gadugadu.Main;

import java.io.IOException;

public class ContactsController {

    @FXML
    private TableView<Contact> contactsTable;
    @FXML
    private TableColumn<Contact, String> nameColumn;
    @FXML
    private TableColumn<Contact, String> statusColumn;
    @FXML
    private TableColumn<Contact, String> numberColumn;

    private ObservableList<Contact> names;


    private TaskNewContact taskNewContact;

    @FXML
    public void initialize() {
        Main.client.contactsController = this;
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        numberColumn.setCellValueFactory(new PropertyValueFactory<>("number"));
        names = FXCollections.observableArrayList(Main.client.getContacts());
        contactsTable.setItems(names);
        taskNewContact = new TaskNewContact();
        Thread thread = new Thread(taskNewContact);
        //thread.setDaemon(false);
        thread.start();


    }

    public void end(){
        Platform.exit();
    }

    public void stopTask() {
        taskNewContact.cancelled();
    }

    public void addNewContact(Contact contact) {

        FilteredList<Contact> sameNames = names.filtered(c -> c.getNumber() == contact.getNumber());
        if (sameNames.isEmpty()) {
            names.add(contact);
            Main.client.addNewContact(contact);
            contactsTable.refresh();
        } else {
            FilteredList<Contact> diffrentStatus = sameNames.filtered(c -> !(c.getStatus().equals(contact.getStatus())));
            if (!diffrentStatus.isEmpty()) {
                Main.client.removeContact(diffrentStatus.get(0));
                names.remove(diffrentStatus.get(0));
                names.add(contact);
                Main.client.addNewContact(contact);
                contactsTable.refresh();
            }
        }
    }

    public void handleStartConversation() {
        Contact contact = contactsTable.getSelectionModel().getSelectedItem();

        contactsTable.refresh();
        if (contact != null) {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getClassLoader().getResource("conversation.fxml"));

            try {
                fxmlLoader.load();
            } catch (IOException e) {
                System.out.println(" bład");
            }


            if (!Main.client.controllerMap.containsKey(contact)) {
                ConversationController controller = fxmlLoader.getController();
                controller.setPerson(contact);
                Main.client.controllerMap.put(contact, controller);
                Parent parent = fxmlLoader.getRoot();

                Scene scene = new Scene(parent, 461, 440);
                Stage stage = new Stage();
                stage.setTitle(String.valueOf(contact.getNumber()));
                stage.setScene(scene);
                stage.setOnHidden(e -> controller.shutdown());
                stage.show();
            }
        }
    }

    public void newConversationWindow(Contact contact) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("conversation.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            System.out.println("bład8");
        }
        ConversationController controller = fxmlLoader.getController();
        controller.setPerson(contact);
        Main.client.controllerMap.put(contact, controller);
        Parent parent = fxmlLoader.getRoot();

        Scene scene = new Scene(parent, 461, 440);
        Stage stage = new Stage();
        stage.setTitle(String.valueOf(contact.getNumber()));
        stage.setScene(scene);
        stage.setOnHidden(e -> controller.shutdown());
        stage.show();
    }


}



