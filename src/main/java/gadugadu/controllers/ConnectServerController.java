package gadugadu.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import gadugadu.Main;

import java.io.IOException;

public class ConnectServerController {

    @FXML
    private TextField domainTextField;
    @FXML
    private TextField passwordTextField;
    @FXML
    private Button connectButton;
    @FXML
    private Label warningLabel;

    public void handleConnectServer(ActionEvent event) throws IOException {
        String domain = new String();
        int port = 0;
        boolean data_good = false;
        boolean connect_done = false;
        try {
            domain = domainTextField.getText();
            port = Integer.parseInt(passwordTextField.getText());
            data_good = true;
        } catch (Exception e) {
            warningLabel.setVisible(true);
            warningLabel.setWrapText(true);
            warningLabel.setText("Błędny format nazwy domeny lub numeru portu!");
        }
        if (data_good == true) {
            try {

                Main.client.setParamConnect(domain, port);
                connect_done = true;

            } catch (Exception e) {
                warningLabel.setVisible(true);
                warningLabel.setWrapText(true);
                warningLabel.setText("Podany serwer nie istnieje lub nie jest obecnie dostępny");
            }
        }

        if (connect_done) {
            Parent parent = FXMLLoader.load(getClass().getClassLoader().getResource("logIn.fxml"));

            Scene scene = new Scene(parent, 400, 400);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setResizable(false);
            stage.setScene(scene);
            stage.setOnCloseRequest(e -> {
                Main.client.getClientRead().endConnection();
                Platform.exit();
            });
            stage.show();
        }

    }

}
