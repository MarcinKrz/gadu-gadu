package gadugadu.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextFormatter;
import lombok.Getter;
import gadugadu.Main;

public class ConversationController {

    @FXML
    private Button sendButton;
    @FXML
    private TextArea yourMessageField;
    @FXML
    private Label nameLabel;
    @FXML
    private TextArea conversationArea;

    @Getter
    private Contact person;

    public void initialize(){
        yourMessageField.setTextFormatter(new TextFormatter<String>(change ->
                change.getControlNewText().length() <2000 ? change : null));
    }

    public void clearConversation() {
        conversationArea.clear();
    }

    public void showNewMessage(String message) {
        conversationArea.appendText(person.name + ":  ");
        conversationArea.appendText(message + "\n");
    }

    @FXML
    public void sendMessage() {
        char[] message = yourMessageField.getText().toCharArray();
        yourMessageField.setText("");
        if (message.length > 0 && message.length < 2000) {
            Main.client.sendMessage(Integer.toString(person.number).toCharArray(), message);
            conversationArea.appendText(Main.client.getMe().name + ":  ");
            conversationArea.appendText(new String(message) + "\n");
        }
    }


    public void shutdown() {

        Main.client.controllerMap.remove(person);
    }

    public void end(){
        Platform.exit();
    }

    public void setPerson(Contact person) {
        this.person = person;
        this.nameLabel.setText(person.getName());

    }
}
