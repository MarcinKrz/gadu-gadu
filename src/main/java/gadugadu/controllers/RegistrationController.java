package gadugadu.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import gadugadu.Main;

import java.io.IOException;

public class RegistrationController {

    @FXML
    private TextField nameTextField;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private Label warningLabel;
    @FXML
    private Button newAccountButton;


    public void initialize() {
        Main.client.registrationController = this;
//        ClientRead clientRead = new ClientRead(Main.client.clientSocket);

    }

    public void createContactWindow(String number) {
        Main.client.setMe(new Contact(Integer.parseInt(number),new String("me"),true));
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("contactstList.fxml"));

        try {
             fxmlLoader.load();
        } catch (IOException e) {
            System.out.println("asdas bład");
        }


        Parent parent = fxmlLoader.getRoot();

        Scene scene = new Scene(parent, 403, 400);
        Stage stage = new Stage();
        stage.setTitle(number);
        stage.setResizable(false);
        stage.setOnCloseRequest(e -> {
            Main.client.contactsController.stopTask();
            Main.client.getClientRead().endConnection();
            Platform.exit();
        });
        stage.setScene(scene);
        stage.show();

        Stage stageLogIn = (Stage) newAccountButton.getScene().getWindow();
        stageLogIn.close();
        Main.client.registrationController=null;
    }

    public void setWarning(String warning) {
        warningLabel.setVisible(true);
        warningLabel.setWrapText(true);
        warningLabel.setText("Nieudana rejstracja!\n" + warning);
    }

    public void end(){
        Platform.exit();
    }


    public void handleRegistration(ActionEvent event) throws IOException {
        boolean data_good = false;
        char[] name = new char[64];
        char[] password = new char[64];

        name = nameTextField.getText().toCharArray();
        password = passwordTextField.getText().toCharArray();

        warningLabel.setText("");
        warningLabel.setVisible(false);

        if (name.length > 64 || name.length == 0) {
            warningLabel.setVisible(true);
            warningLabel.setWrapText(true);
            warningLabel.setText("Nieudana rejestracja!\nTwoja nazwa może mieć maksymalnie 64 znaki");
        } else if (password.length > 64 || password.length == 0) {
            warningLabel.setVisible(true);
            warningLabel.setWrapText(true);
            warningLabel.setText("Nieudana rejstracja!\nMaksymalna długość hasła powinna wynosić 64 znaki");
        } else
            data_good = true;



        if (data_good) {
            Main.client.registration(name, password);
        }

    }
}
