package gadugadu;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import gadugadu.controllers.Contact;

import java.util.ArrayList;

public class Main extends Application {

    static final public Client client = new Client();

    @Override
    public void start(Stage primaryStage) throws Exception {


        ArrayList<Contact> a = new ArrayList<Contact>();
        client.setContacts(a);

        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("connectServer.fxml"));
        primaryStage.setTitle("Gadu-Gadu");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 400, 400));
        primaryStage.show();
    }


    public static void main(String[] args) {

        launch(args);
    }

}
