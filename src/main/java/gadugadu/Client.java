package gadugadu;

import javafx.application.Platform;
import lombok.Getter;
import lombok.Setter;
import gadugadu.controllers.*;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Client extends Thread implements Serializable {

    private Socket clientSocket;
    @Getter
    @Setter
    private List<Contact> contacts = new ArrayList<Contact>();

    @Getter
    @Setter
    private Contact me;

    @Getter
    @Setter
    private ClientRead clientRead;

    public Map<Contact, ConversationController> controllerMap = new HashMap<Contact, ConversationController>();
    public RegistrationController registrationController;
    public LogInController logInController;
    public ContactsController contactsController;

    /**
     * metoda ustawiająca parametry serwera
     * @param host nazwa domeny serwera
     * @param port numer portu serwera
     * @throws Exception
     */
    public void setParamConnect(String host, int port) {
        try {
            clientSocket = new Socket(host, port);
        } catch (IOException e) {
        }
        clientRead = new ClientRead(clientSocket);
        clientRead.start();

    }

    public void addNewContact(Contact contact) {
        contacts.add(contact);
    }



    public  void removeContact(Contact contact){
        contacts.remove(contact);
    }

    /**
     * metoda wysyłająca zapytanie o liste kontaktów do serwera
     */
    public void requestContacts() {
        char[] protRegistr = new char[2015];
        protRegistr[0] = (char) 2;
        protRegistr[1] = '2';
        char[] myNumber = Integer.toString(me.getNumber()).toCharArray();
        for (int i = 2; i <= 7; i++) {
            protRegistr[i] = myNumber[i - 2];
        }
        protRegistr[2014] = (char) 25;

        try {
            OutputStream output = clientSocket.getOutputStream();
            output.write(String.valueOf(protRegistr).getBytes());
        } catch (IOException e) {
            if(Main.client.registrationController!=null){
               Platform.runLater(() -> Main.client.registrationController.end()) ;
            }
            if(!Main.client.controllerMap.isEmpty()){
                Main.client.controllerMap.forEach((c,controller) -> Platform.runLater(() -> controller.end()));
            }
            if(Main.client.logInController!=null){
                Platform.runLater(() -> Main.client.logInController.end()) ;
            }
            if(Main.client.contactsController!=null){
                Platform.runLater(() -> Main.client.contactsController.end()) ;
            }
        }

    }

    /**
     * metoda wysyłająca do serwera wiadomość o próbie logowania
     * @param numberLog login uzytkownika
     * @param password haslo uzytkownia
     */
    public void logIn(char[] numberLog, char[] password) {
        char[] protRegistr = new char[138];
        protRegistr[0] = (char) 2;
        protRegistr[1] = 'l';
        protRegistr[2] = '0';
        for (int i = 3; i <= 8; i++) {
            protRegistr[i] = numberLog[i - 3];
        }
        for (int i = 73; i < 73 + password.length; i++) {
            protRegistr[i] = password[i - 73];
        }
        protRegistr[137] = (char) 25;

        try {

            OutputStream output = clientSocket.getOutputStream();
            output.write(String.valueOf(protRegistr).getBytes());

        } catch (IOException e) {
            if(Main.client.registrationController!=null){
                Platform.runLater(() -> Main.client.registrationController.end()) ;
            }
            if(!Main.client.controllerMap.isEmpty()){
                Main.client.controllerMap.forEach((c,controller) -> Platform.runLater(() -> controller.end()));
            }
            if(Main.client.logInController!=null){
                Platform.runLater(() -> Main.client.logInController.end()) ;
            }
            if(Main.client.contactsController!=null){
                Platform.runLater(() -> Main.client.contactsController.end()) ;
            }
        }

    }

    /**
     * metoda wysyłająca do serwera wiadomość o próbie rejestracji
     * @param name podana przez uzytkownika nazwa
     * @param password haslo uzytkownika
     * @return
     */
    public int registration(char[] name, char[] password) {
        char[] protRegistr = new char[138];
        protRegistr[0] = (char) 2;
        protRegistr[1] = 'r';
        protRegistr[2] = '0';
        for (int i = 3; i <= 8; i++) {

        }
        for (int i = 9; i < 9 + name.length; i++) {
            protRegistr[i] = name[i - 9];
        }
        for (int i = 9 + name.length; i <= 72; i++) {
            protRegistr[i] = (char) 0;
        }
        for (int i = 73; i < 73 + password.length; i++) {
            protRegistr[i] = password[i - 73];
        }
        for (int i = 73 + password.length; i <= 136; i++) {
            protRegistr[i] = (char) 0;
        }
        protRegistr[137] = (char) 25;
        try {

            OutputStream output = clientSocket.getOutputStream();
            output.write(String.valueOf(protRegistr).getBytes());


        } catch (IOException e) {
            if(Main.client.registrationController!=null){
                Platform.runLater(() -> Main.client.registrationController.end()) ;
            }

            if(!Main.client.controllerMap.isEmpty()){
                Main.client.controllerMap.forEach((c,controller) -> Platform.runLater(() -> controller.end()));
            }
            if(Main.client.logInController!=null){
                Platform.runLater(() -> Main.client.logInController.end()) ;
            }
            if(Main.client.contactsController!=null){
                Platform.runLater(() -> Main.client.contactsController.end()) ;
            }
        }
        return 1;
    }

    /**
     * metoda wysyłająca do serwera treść wiadomości do innego użytkownika gadu-gadu
     * @param numberTo numer odbiorcy
     * @param message   tresc wiadomosci
     */
    public void sendMessage(char[] numberTo, char[] message) {
        char[] protRegistr = new char[2015];
        protRegistr[0] = (char) 2;
        protRegistr[1] = '1';
        char[] myNumber = Integer.toString(me.getNumber()).toCharArray();
        for (int i = 2; i <= 7; i++) {
            protRegistr[i] = myNumber[i - 2];
        }
        for (int i = 8; i <= 13; i++) {
            protRegistr[i] = numberTo[i - 8];
        }
        for (int i = 14; i < 14 + message.length; i++) {
            protRegistr[i] = message[i - 14];
        }
        protRegistr[2014] = (char) 25;

        try {
            OutputStream output = clientSocket.getOutputStream();
            output.write(String.valueOf(protRegistr).getBytes());
        } catch (IOException e) {
            if(Main.client.registrationController!=null){
                Platform.runLater(() -> Main.client.registrationController.end()) ;
            }
            if(!Main.client.controllerMap.isEmpty()){
                Main.client.controllerMap.forEach((c,controller) -> Platform.runLater(() -> controller.end()));
            }
            if(Main.client.logInController!=null){
                Platform.runLater(() -> Main.client.logInController.end()) ;
            }
            if(Main.client.contactsController!=null){
                Platform.runLater(() -> Main.client.contactsController.end()) ;
            }
        }

    }

    /**
     * metoda konczaca połączenie  z serwerem
     */
    public void closeConnection() {
        try {
            clientSocket.close();
        } catch (IOException e) {
        }
    }


}

